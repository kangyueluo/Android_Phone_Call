package example.givemepass.calldemo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity

import android.util.Log
import android.widget.Button


class PhoneCall: AppCompatActivity()
{
    private val TAG :String = "PhoneCall"
    private val CALL :String = "android.intent.action.CALL"
    private var btnCall : Button? = null
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE : Int = 100;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handlePermission()

        btnCall = findViewById(R.id.call) as Button?
        btnCall!!.setOnClickListener {

            var call :Intent = Intent(CALL, Uri.parse("tel:" + "0987654321"))
            startActivity(call)

        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onRestart() {
        super.onRestart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun handlePermission()
    {
        val permission : Int = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)

        if(permission != PackageManager.PERMISSION_GRANTED)
        {
            Log.d(TAG,"CALL_PHONE PERMISSION_DENIED");

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), MY_PERMISSIONS_REQUEST_CALL_PHONE)
        }
        else {
            Log.d(TAG, "CALL_PHONE PERMISSION_GRANTED")
        }
    }


}
